PROJECT_NAME=project
TEST=.
BENCH=.
COVERPROFILE=/tmp/c.out
BRANCH=`git rev-parse --abbrev-ref HEAD`
COMMIT=`git rev-parse --short HEAD`
CWD=`pwd`
GOLDFLAGS="" #-X main.branch $(BRANCH) -X main.commit $(COMMIT)"
GO=go
GOPATH=$(CWD)/.gopath
GOBUILDFLAGS="-a"
GOGCFLAGS=""
CGO_CFLAGS=""
CGO_LDFLAGS=""

default: build

clean:
	@rm -f bin/*

# http://cloc.sourceforge.net/
cloc:
	@cloc --by-file-by-lang --exclude-dir=.git,env32,env33,env34,.gopath --not-match-f='Makefile|_test.go' .

fmt:
	@GOPATH=$(GOPATH) $(GO) fmt ./...

get:
	@GOPATH=$(GOPATH) $(GO) get -v golang.org/x/tools/cmd/cover
	@GOPATH=$(GOPATH) $(GO) get -v golang.org/x/tools/cmd/vet
	@GOPATH=$(GOPATH) $(GO) get -v github.com/davecheney/gcvis
	@GOPATH=$(GOPATH) $(GO) get -a -d -v ./...

get-update:
	@GOPATH=$(GOPATH) $(GO) get -a -u -v golang.org/x/tools/cmd/cover
	@GOPATH=$(GOPATH) $(GO) get -a -u -v golang.org/x/tools/cmd/vet
	@GOPATH=$(GOPATH) $(GO) get -a -u -v github.com/davecheney/gcvis
	@GOPATH=$(GOPATH) $(GO) get -a -u -d -v ./...

gopath:
	@mkdir -p $(GOPATH)/src/bitbucket.org/widefido
	@ln -s ../../../../ $(GOPATH)/src/bitbucket.org/widefido/$(PROJECT_NAME)

bench: fmt
	@echo "=== BENCHMARKS ==="
	GOPATH=$(GOPATH) $(GO) test -v -test.run=NOTHINCONTAINSTHIS -test.bench=$(BENCH) -test.benchmem -test.benchtime "2s" ./...

test: fmt
	@echo "=== TESTS ==="
	@GOPATH=$(GOPATH) $(GO) test -v -cover -test.run=$(TEST) ./...

cover: fmt
	GOPATH=$(GOPATH) $(GO) test -coverprofile=$(COVERPROFILE) -test.run=$(TEST) $(COVERFLAG) .
	GOPATH=$(GOPATH) $(GO) tool cover -html=$(COVERPROFILE)
	rm $(COVERPROFILE)

vet:
	GOPATH=$(GOPATH) $(GO) vet ./...

todo:
	@git grep --untracked TODO

build: get fmt
	@mkdir -p bin
	@GOPATH=$(GOPATH) $(GO) build $(GOBUILDFLAGS) -gcflags=$(GOGCFLAGS) -ldflags=$(GOLDFLAGS) -o bin/$(PROJECT_NAME) ./cmd/$(PROJECT_NAME)

all: build

.PHONY: clean bench cloc cover fmt get build test
